import autograd.numpy as np


def rbf_covariance(X, y=None, signal_variance=1.0, length_scale=1.0):
    if y is None:
        y = X

    D = np.expand_dims(X / length_scale, 1) - np.expand_dims(y / length_scale, 0)

    return signal_variance * np.exp(-0.5 * np.sum(D ** 2, axis=2))

# class ARD(object):
#     def __init__(self, length_scale=1.0, signal_variance=1.0, ):
#         self.length_scale = 1.0
