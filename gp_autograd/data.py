import numpy as np
from sklearn.model_selection import train_test_split


def example_1d(func=1, x_error=0.3):
    seed = 123
    rng = np.random.RandomState(seed=seed)

    # sample data parameters
    n_train, n_test, n_trial = 60, 100, 2000
    sigma_y = 0.05
    x_cov = x_error
    x_min, x_max = -10, 10

    # real function
    if func == 1:
        f = lambda x: np.sin(1.0 * np.pi / 1.6 * np.cos(5 + .5 * x))
    elif func == 2:
        f = lambda x: np.sinc(x)

    else:
        f = lambda x: np.sin(2. * x) + np.exp(0.2 * x)

    # Training add x, y = f(x)
    x = np.linspace(x_min, x_max, n_train + n_test)

    x, xs, = train_test_split(x, train_size=n_train, random_state=seed)

    # add noise
    y = f(x)
    x_train = x + x_cov * rng.randn(n_train)
    y_train = f(x) + sigma_y * rng.randn(n_train)

    x_train, y_train = x_train[:, np.newaxis], y_train[:, np.newaxis]

    # -----------------
    # Testing Data
    # -----------------

    ys = f(xs)

    # Add noise
    x_test = xs + x_cov * rng.randn(n_test)
    y_test = ys

    x_test, y_test = x_test[:, np.newaxis], y_test[:, np.newaxis]

    # -------------------
    # Plot Points
    # -------------------
    x_plot = np.linspace(x_min, x_max, n_test)[:, None]
    y_plot = f(x_plot)

    X = {
        'train': x_train,
        'test': x_test,
        'plot': x_plot
    }
    y = {
        'train': y_train,
        'test': y_test,
        'plot': y_plot
    }

    error_params = {
        'x': x_cov,
        'y': sigma_y,
        'f': f
    }

    return X, y, error_params



def example_1d(func=1):
    seed = 123
    rng = np.random.RandomState(seed=seed)

    # sample data parameters
    n_train, n_test, n_trial = 60, 100, 2000
    sigma_y = 0.05
    x_cov = 0.3
    y_scale = .5
    x_min, x_max = -10, 10

    # real function
    if func == 1:
        f = lambda x: np.sin(1.0 * np.pi / 1.6 * np.cos(5 + y_scale * x))
    elif func == 2:
        f = lambda x: np.sinc(x)

    else:
        f = lambda x: np.sin(2. * x) + np.exp(0.2 * x)

    # Training add x, y = f(x)
    x = np.linspace(x_min, x_max, n_train + n_test)

    x, xs, = train_test_split(x, train_size=n_train, random_state=seed)

    # add noise
    y = f(x)
    x_train = x + x_cov * rng.randn(n_train)
    y_train = f(x) + sigma_y * rng.randn(n_train)

    x_train, y_train = x_train[:, np.newaxis], y_train[:, np.newaxis]

    # -----------------
    # Testing Data
    # -----------------

    ys = f(xs)

    # Add noise
    x_test = xs + x_cov * rng.randn(n_test)
    y_test = ys

    x_test, y_test = x_test[:, np.newaxis], y_test[:, np.newaxis]

    # -------------------
    # Plot Points
    # -------------------
    x_plot = np.linspace(x_min, x_max, n_test)[:, None]
    y_plot = f(x_plot)

    X = {
        'train': x_train,
        'test': x_test,
        'plot': x_plot
    }
    y = {
        'train': y_train,
        'test': y_test,
        'plot': y_plot
    }

    error_params = {
        'x': x_cov,
        'y': sigma_y
    }

    return X, y, error_params


def sample_data():
    """Gets some sample data."""
    d_dimensions = 1
    n_samples = 20
    noise_std = 0.1
    seed = 123
    rng = np.random.RandomState(seed)


    n_train = 20
    n_test = 1000
    xtrain = np.linspace(-4, 5, n_train).reshape(n_train, 1)
    xtest = np.linspace(-4, 5, n_test).reshape(n_test, 1)

    f = lambda x: np.sin(x) * np.exp(0.2 * x)
    ytrain = f(xtrain) + noise_std * rng.randn(n_train, 1)
    ytest = f(xtest)

    return xtrain, xtest, ytrain, ytest

