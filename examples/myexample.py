



def test_error():
    X, y, error_params = example_1d()
    # Initialize GP Model
    gp_autograd = GaussianProcessError()


    # Fit GP Model
    gp_autograd.fit(X['train'], y['train'])
    # Make Predictions
    y_pred, y_var = gp_autograd.predict(X['test'], return_std=True)
    pass


def test_original():
    # Get sample data
    xtrain, xtest, ytrain, ytest = sample_data()

    # Initialize GP Model
    gp_autograd = GaussianProcess()

    # Fit GP Model
    gp_autograd.fit(xtrain, ytrain)

    # Make Predictions
    y_pred, y_var = gp_autograd.predict(xtest, return_std=True)


    ##########################
    # First Derivative
    ##########################


    # Autogradient
    mu_der = gp_autograd.mu_grad(xtest)

    # # Numerical Gradient
    num_grad = np_gradient(y_pred, xtest)

    assert (mu_der.shape == num_grad.shape)

    # Plot Data
    fig, ax = plt.subplots(figsize=(10, 7))

    ax.scatter(xtrain, ytrain, color='r', label='Training Noise')
    ax.plot(xtest, y_pred, color='k', label='Predictions')
    ax.plot(xtest, mu_der, color='b', linestyle=":", label='Autograd 1st Derivative')
    ax.plot(xtest, num_grad, color='y', linestyle="--", label='Numerical Derivative')

    ax.legend()
    plt.show()

    ############################
    # 2nd Derivative
    ############################
    mu_der2 = gp_autograd.mu_grad(xtest, nder=2)
    num_grad2 = np_gradient(num_grad, xtest)

    # Plot
    fig, ax = plt.subplots(figsize=(10, 7))

    # ax.scatter(xtrain, ytrain)
    ax.scatter(xtrain, ytrain, color='r', label='Training Points')
    ax.plot(xtest, y_pred, color='k', label='Predictions')
    ax.plot(xtest, mu_der2, color='b', linestyle=":", label='Autograd 2nd Derivative')
    ax.plot(xtest, num_grad2, color='y', linestyle="--", label='Numerical 2nd Derivative')

    ax.legend()

    plt.show()

    assert (mu_der2.all() == num_grad2.all())